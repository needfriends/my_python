# first you need Install pip3 install simple json
# os:to check if the file exit or not exit
import simplejson as json
import os

#Check the file exists  and check the file size
if os.path.isfile("./ages.json") and os.stat("./ages.json").st_size != 0:
  old_file = open("./ages.json","r+")
  #Transform the text in json
  data = json.load(old_file)

  print("Current age is ",data["age"], "---- adding year.")
  data["age"] = data["age"] +1
  print("New age is ",data["age"])
else:
    old_file = open("./ages.json","w+")
    data = {"name":"nick","age":27}
    print("No file found,setting default age to data",data["age"])

old_file.seek(0)
#Write in json
old_file.write(json.dumps(data))



